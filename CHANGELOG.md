## [4.3.0] – 2023-07-11

### Added
- `Zenject` optional integration, that featuring `ScreenManagerInstaller` installer script
- `ScreenSystemConfig` with combined configuration
- It's possible now to use main `Camera` instead of selected one

### Changed
- Editors UI now looks better
- `README.md` update

### Removed
- `ScreenLayerOrder` and `ScreenPrefabMap` are not SO anymore, use `ScreenSystemConfig` insted


## [4.2.1] – 2023-07-10

### Fixed
- Editor links


## [4.2.0] – 2023-07-04

### Added
- It's now possible to set initial UI focus object 

### Fixed
- On screen display, UI focus is cleared, so it's not possible to navigate on hidden screen now 

### Changed
- CI/CD updated for modularity


## [4.1.1]

### Fixed
- Forced layer now stored in screen instance


## [4.1.0]

### Added
- If screen arg is `IDisposable` – it would be disposed after screen destroy 

### Fixed
- Can't show already showed screen with different argument without closing anymore
- Asset creation naming


## [4.0.0]

### Added
- BREAKING: `ShowScreen()` and `HideScreen()` methods are now sync
- You can pass argument to `ShowScreen()`, so initialization of screens are parametrized
- You can separately await animation execution on `ShowScreen()` and `HideScreen`

### Changed
- BREAKING: Canvas layer management is now handled by class `LayerManager`
- BREAKING: `GetScreen` thows `NullReferenceException`, if no screen presented on scene
- `ScreenState` renamed to `ScreenDisplayStage`
- `ScreenInfo` renamed to `ScreenState` (sorry for inconvenience)


## [3.4.0]

### Added
- It's now possible to force-display screen on particular layer


## [3.3.1]

### Changed
- Dependency to Valhalla Serialization renamed


## [3.3.0]

### Added
- Events for showing and hiding added

### Changed
- Rebranded from "Open Unity Solutions" to "Valhalla" (breaking change, affects namespaces and package domain)

### Removed
- Unused method `GetAlreadyShowingScreen` deleted


## [3.2.0]

### Added
- You can despawn directly from prefabs now, without generic-based search


## [3.1.0]

### Added
- You can spawn directly from prefabs now, without generic-based search


## [3.0.1]

### Added
- Offset for sorting layers

### Changed
- Slightly changed changelog formatting


## [3.0.0]

### Added
- Tracking of screen state

### Changed
- You can't show screen twice
- You now can "hide" screen if it's already hidden without exception

### Fixed
- Fixed race condition on screen hide and show


## [2.0.1]

### Fixed
- `[Required]` for scene dependencies replaced with `[RequiredIn]` for Odin Validator


## [2.0.0]

### Added
- Layers order is configurable

### Changed
- Layers now defined by `ScriptableObject`s
- `Canvas`es now generated for layers in runtime if needed
- Several screens can be located in one layer


### Misc
- Odin's `Serialized...` inheritance removed


## [1.0.0]

### Features
- Package created
