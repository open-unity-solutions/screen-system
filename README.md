# Valhalla: Screen System
## About

Screen system. Multi-layered and async.

## How to use

### Quick start

1. Code a screen class, inherit it from `BaseScreen`

    To add opening and closing animations, override `.OnShow` and `.OnHide` methods.

2. Create a `ScreenSystemConfig`, link your screen to screen list in the inspector.

3. Create one or more `ScreenLayer`s.

4. Setup order of layers in `ScreenSystemConfig`.

5. On scene, add empty object and add `ScreenManager` component on it.

6. Add the canvas prefab, that will be used for layers.

7. Link your canvas and configs in `ScreenManager` object.


For all "Create" operations you can use context menu `Create -> Open Unity Solutions -> Screen System -> XXX`

If you want add pre-defined canvases on scene – run context menu `Setup State` after that to setup sorting orders.


### Strong typed parameters

If you want to pass parameters to screen immediately after instantiation, you need to inherits your screen class not from `BaseScreen`, but from `BaseScreen<TParameterType>`.\
Keep in mind order of execution:
1. `.Awake()`
2. `.SetupOnInstantiate()`/`.SetupOnInstantiate(myParameter)`
3. `Start()`


### Animations

You can write some async code for opening and closing animations of screens by overriding `.PlayShowAnimationAsync()` and `.PlayHideAnimationAsync()` methods.


## Dependencies:
- `UniTask`
- `OpenUnitySolutions.UnityDictionary`
- `Odin`


## Third Party Integrations:
### Zenject

Zenject integration enables automatically via [version defines](https://docs.unity3d.com/Manual/ScriptCompilationAssemblyDefinitionFiles.html#define-symbols) based on presence of `com.svermeulen.extenject` package in project.


Keep in mind, that Unity Asset Store version **is not a package dependency**. You can reference a git dependency from any stable Zenject fork, [this](https://github.com/Mathijs-Bakker/Extenject.git) looks decent.

Alternatively, you can force integration enable by defining `ZENJECT_ENABLED` symbol in your project. 

## TODO for v2:
- [ ] Make Odin integration optional
- [ ] Add examples
- [ ] Info about layer changes
- [ ] Info about screen display states
- [x] Write better readme
- [x] Add layer-canvas generator
- [x] Make custom amount of layers
