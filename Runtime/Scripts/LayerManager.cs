using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;
using Valhalla.ScreenSystem.State;
using Valhalla.Serialization;
using Object = UnityEngine.Object;


#if UNITY_EDITOR
using UnityEditor;
#endif


namespace Valhalla.ScreenSystem
{
	[Serializable]
	public class LayerManager
	{
		[SerializeField, SceneObjectsOnly, RequiredIn(PrefabKind.InstanceInScene)]
		private Transform _canvasRoot;


		[SerializeField]
		private bool _useMainCamera = true;
		
		
		[SerializeField, SceneObjectsOnly, Required]
		[HideIf(nameof(_useMainCamera))]
		private Camera _canvasCamera;


		[SerializeField, AssetsOnly, Required]
		private Canvas _referenceCanvasPrefab;

		[SerializeField]
		private UnityDictionary<ScreenLayer, Canvas> _layers = new();


		public Canvas GetLayerCanvas(ScreenLayer layer, ScreenSystemConfig config)
		{
			if (!_layers.ContainsKey(layer) || _layers[layer] == null)
				return AddLayer(layer, config);

			return _layers[layer];
		}


		private Canvas AddLayer(ScreenLayer layer, ScreenSystemConfig config)
		{
			if (_layers.ContainsKey(layer) && _layers[layer] != null)
				throw new IndexOutOfRangeException("Layer already instantiated");

			var canvas = Object.Instantiate(_referenceCanvasPrefab.gameObject, _canvasRoot)
				.GetComponent<Canvas>();

			canvas.sortingOrder = config.ScreenLayerOrder[layer];
			canvas.gameObject.name = $"{char.ToUpper(layer.name[0]) + layer.name.Substring(1)} Layer Canvas";
			canvas.worldCamera = _useMainCamera
				? Camera.main
				: _canvasCamera;

			_layers.Add(layer, canvas);

			return canvas;
		}


		#if UNITY_EDITOR


		public List<ScreenState> SetupExistingScene(ScreenSystemConfig config)
		{
			SetupSortingLayers(config);

			return GetInstantiatedScreens();
		}


		private void SetupSortingLayers(ScreenSystemConfig config)
		{
			foreach (var (layer, canvas) in _layers)
			{
				if (canvas == null)
					continue;

				canvas.sortingOrder = config.ScreenLayerOrder[layer];
				EditorUtility.SetDirty(canvas);
			}
		}


		private List<ScreenState> GetInstantiatedScreens()
		{
			var result = new List<ScreenState>();

			foreach (var canvas in _layers.Values)
			{
				if (canvas == null)
					continue;

				var screens = canvas.GetComponentsInChildren<NoParameterInfoScreen>(true);

				result.AddRange(screens.Select(screen => new ScreenState(screen, ScreenDisplayStage.ShowedAndDisplaying)));
			}

			return result;
		}


		#endif
	}
}
