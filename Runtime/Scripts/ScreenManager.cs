﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using Valhalla.UniTaskExtension;
using Valhalla.Serialization;
using Valhalla.Serialization.Extensions;
using Valhalla.ScreenSystem.State;
using Sirenix.OdinInspector;


namespace Valhalla.ScreenSystem
{
	public class ScreenManager : MonoBehaviour
	{
		public readonly UnityEvent<NoParameterInfoScreen> OnScreenCreate = new();

		public readonly UnityEvent<NoParameterInfoScreen> BeforeScreenDestroy = new();
		
		[SerializeField, AssetsOnly, Required]
		[BoxGroup("Setup")]
		private ScreenSystemConfig _config;


		[SerializeField, SceneObjectsOnly, Required]
		[BoxGroup("Setup/Scene")]
		private UnityDictionary<string, ScreenState> _screens;

		
		[SerializeField, Required]
		[BoxGroup("Setup/Scene"), HideLabel]
		private LayerManager _layers = new();


		[Button]
		public TScreen ShowScreen<TScreen>(ScreenLayer forceLayer = null)
			where TScreen : BaseScreen
		{
			var screenType = typeof(TScreen);
		
			BaseScreen GetPrefabInstanceCallback(ScreenLayer layer) 
				=> SpawnScreen<TScreen>(layer);
			
			return (TScreen) ShowScreen(screenType, GetPrefabInstanceCallback, forceLayer);
		}
		
		
		[Button]
		public TScreen ShowScreen<TScreen, TArg>(TArg arg, ScreenLayer forceLayer = null)
			where TScreen : BaseScreen<TArg>
		{
			var screenType = typeof(TScreen);
		
			BaseScreen<TArg> GetPrefabInstanceCallback(ScreenLayer layer) 
				=> SpawnScreen<TScreen>(layer);
			
			return (TScreen) ShowScreen(screenType, arg, GetPrefabInstanceCallback, forceLayer);
		}


		public NoParameterInfoScreen ShowScreen(BaseScreen screenPrefab, ScreenLayer forceLayer = null)
		{
			var screenType = screenPrefab.GetType();

			BaseScreen GetPrefabInstanceCallback(ScreenLayer layer) 
				=> SpawnScreenFromPrefab(screenPrefab, layer);

			return ShowScreen(screenType, GetPrefabInstanceCallback, forceLayer);
		}


		public NoParameterInfoScreen ShowScreen<TArg>(BaseScreen<TArg> screenPrefab, TArg arg, ScreenLayer forceLayer = null)
		{
			var screenType = screenPrefab.GetType();

			BaseScreen<TArg> GetPrefabInstanceCallback(ScreenLayer layer) 
				=> SpawnScreenFromPrefab(screenPrefab, layer);

			return ShowScreen(screenType, arg, GetPrefabInstanceCallback, forceLayer);
		}
		
		
		private NoParameterInfoScreen ShowScreen(Type screenType, Func<ScreenLayer, BaseScreen> getPrefabInstanceCallback, [CanBeNull] ScreenLayer forceLayer)
		{
			var screenState = GetOrCreateState(screenType);

			if (screenState.DisplayStage != ScreenDisplayStage.Hidden)
				return ShowScreenThatAlreadyShown(screenType, screenState, forceLayer, null);

			
			Debug.Log($"Screens | Showing fresh created screen <{screenType.Name}>");
					
			var screenInstance = getPrefabInstanceCallback.Invoke(forceLayer);
					
			screenInstance.SetupOnInstantiate();
			screenInstance.FocusOnScreen();
			OnScreenCreate.Invoke(screenInstance);
					
			PlayShowScreenAsync(screenState, screenInstance).ForgetWithHandler();
			return screenInstance;
		}
		
		
		
		private NoParameterInfoScreen ShowScreen<TArg>(Type screenType, TArg arg, Func<ScreenLayer, BaseScreen<TArg>> getPrefabInstanceCallback, [CanBeNull] ScreenLayer forceLayer)
		{
			var screenState = GetOrCreateState(screenType);

			if (screenState.DisplayStage != ScreenDisplayStage.Hidden)
				return ShowScreenThatAlreadyShown(screenType, screenState, forceLayer, arg);

			screenState.Arg = arg;

			Debug.Log($"Screens | Showing fresh created screen <{screenType.Name}> with arg <{arg}>");
					
			var screenInstance = getPrefabInstanceCallback.Invoke(forceLayer);
					
			screenInstance.SetupOnInstantiate(arg);
			screenInstance.FocusOnScreen();
			OnScreenCreate.Invoke(screenInstance);
					
			PlayShowScreenAsync(screenState, screenInstance).ForgetWithHandler();
			return screenInstance;

		}


		private NoParameterInfoScreen ShowScreenThatAlreadyShown(Type screenType, ScreenState screenState, [CanBeNull] ScreenLayer forceLayer, [CanBeNull] object arg)
		{
			switch (screenState.DisplayStage)
			{
				case ScreenDisplayStage.Hidden:
					throw new InvalidOperationException($"Screens | Screen <{screenType.Name}> is not shown yet, wrong method");
				
				case ScreenDisplayStage.InProcessOfShow:
					Debug.LogWarning($"Screens | Showing screen <{screenType.Name}> that is already in process of showing");

					if (arg != screenState.Arg)
						throw new ArgumentException($"Trying to show the screen with arg <{arg}>, while same screen with arg <{screenState.Arg} is in process of showing>");
					
					ChangeScreenLayer(screenState.Screen, forceLayer);
					return screenState.Screen;
				case ScreenDisplayStage.ShowedAndDisplaying:
					Debug.LogWarning($"Screens | Requested to show screen {screenType}, that is already showed");
					
					if (arg != screenState.Arg)
						throw new ArgumentException($"Trying to show the screen with arg <{arg}>, while same screen with arg <{screenState.Arg} is in process of showing>");

					
					ChangeScreenLayer(screenState.Screen, forceLayer);
					return screenState.Screen;

				case ScreenDisplayStage.InProcessOfHide:
					throw new InvalidOperationException($"Screens | Trying to show screen {screenType} that is in process of hiding");

				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		


		[Button]
		[DisableInEditorMode, BoxGroup("Runtime")]
		private void ChangeScreenLayer(NoParameterInfoScreen screen, [CanBeNull] ScreenLayer newLayer)
		{
			if (screen == null)
			{
				Debug.LogError("Screens | Screen is null, can't change layer");
				return;
			}
			
			var layer = newLayer != null 
				? newLayer
				: screen.Layer;
			var layerCanvas = _layers.GetLayerCanvas(layer, _config);
			
			if (screen.transform.parent == layerCanvas.transform)
				return;
			
			screen.transform.SetParent(layerCanvas.transform);
			screen.FocusOnScreen();
		}


		
		
		private TScreen SpawnScreen<TScreen>([CanBeNull] ScreenLayer forceLayer)
			where TScreen : NoParameterInfoScreen
		{
			var prefab = _config.ScreenPrefabMap.GetPrefab<TScreen>();

			return SpawnScreenFromPrefab(prefab, forceLayer);
		}


		private TScreen SpawnScreenFromPrefab<TScreen>(TScreen prefab, [CanBeNull] ScreenLayer forceLayer)
			where TScreen : NoParameterInfoScreen
		{
			var layer = forceLayer != null 
				? forceLayer 
				: prefab.Layer;
			
			var layerCanvas = _layers.GetLayerCanvas(layer, _config);
            
			var instance = Instantiate(prefab, layerCanvas.transform);
			
			if (forceLayer != null )
				instance.SetForcedLayer(forceLayer);

			return instance;
		}
		

		[Button]
		[DisableInEditorMode, BoxGroup("Runtime")]
		public void HideScreen(NoParameterInfoScreen instanceOrPrefab)
			=> HideScreen(instanceOrPrefab.GetType());


		public void HideScreen<TScreen>()
			where TScreen : NoParameterInfoScreen
			=> HideScreen(typeof(TScreen));
		
		
		private void HideScreen(Type screenType)
		{
			Debug.Log($"Screens | Hiding screen <{screenType.Name}>");

			var screenInfo = GetOrCreateState(screenType);
			
			if (screenInfo.Screen == null)
				throw new NullReferenceException($"Screens | No screen {screenType} found in scene");
			
			if (screenInfo.DisplayStage == ScreenDisplayStage.Hidden)
				return;

			if (screenInfo.DisplayStage == ScreenDisplayStage.InProcessOfHide)
				return;

			if (screenInfo.DisplayStage == ScreenDisplayStage.InProcessOfShow)
				throw new InvalidOperationException($"Screens | Trying to hide screen {screenType} that is in process of showing");

			HideAndDestroyScreenAsync(screenInfo).ForgetWithHandler();
		}
		
		
		private async UniTask PlayShowScreenAsync(ScreenState state, NoParameterInfoScreen screen)
		{
			state.OnShowStarted(screen);
			await screen.ShowAnimationAsync();
			state.OnShowFinished();
		}


		private async UniTask HideAndDestroyScreenAsync(ScreenState state)
		{
			var screen = state.Screen;
			
			state.OnHideStarted();
			await screen.HideAnimationAsync().SuppressCancellationThrow();
			state.OnHideFinished();

			if (screen != null)
			{
				BeforeScreenDestroy.Invoke(screen);
				Destroy(screen.gameObject);
				
				if (state.Arg is IDisposable disposableArg)
					disposableArg.Dispose();
			}
		}


		public TScreen GetScreen<TScreen>() 
			where TScreen : NoParameterInfoScreen
			=> (TScreen) GetOrCreateInfo<TScreen>().Screen;


		private ScreenState GetOrCreateInfo<TScreen>()
			where TScreen : NoParameterInfoScreen
			=> GetOrCreateState(typeof(TScreen));


		private ScreenState GetOrCreateState(Type screenType)
		{
			var screenTypeKey = screenType.FullName;
			
			if (string.IsNullOrEmpty(screenTypeKey))
				throw new NullReferenceException();

			if (_screens.ContainsKey(screenTypeKey))
				return _screens[screenTypeKey];

			var info = new ScreenState();
			_screens.Add(screenTypeKey, info);
			return info;
		}
		
		
		#if UNITY_EDITOR

		[Button("Setup State"), DisableInPlayMode]
		[BoxGroup("Setup")]
		private void Setup()
		{
			var screens = _layers.SetupExistingScene(_config);
			_screens = GetInstantiatedScreensByType(screens);
			
			UnityEditor.EditorUtility.SetDirty(this);
			UnityEditor.AssetDatabase.SaveAssets();
		}
        
		
		private UnityDictionary<string, ScreenState> GetInstantiatedScreensByType(List<ScreenState> screens)
			=> screens
				.ToDictionary(info => info.Screen.GetType().FullName)
				.ToUnityDictionary();
		
		#endif

	}
}
