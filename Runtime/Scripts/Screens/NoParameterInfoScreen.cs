using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;


namespace Valhalla.ScreenSystem
{
	public abstract class NoParameterInfoScreen : MonoBehaviour, ISelfValidator
	{
		[SerializeField, AssetsOnly, Required]
		private ScreenLayer _layer;


		[ShowInInspector, ReadOnly]
		private bool _isInAnimation = false;
		
		public ScreenLayer Layer
			=> _layer;


		protected virtual GameObject DefaultFocusTarget
			=> null;
		
		
		protected virtual bool IgnoreEmptyDefaultFocusTargetWarning
			=> false;


		internal void SetForcedLayer(ScreenLayer value)
		{
			_layer = value;
		}


		public void FocusOnScreen()
		{
			EventSystem.current.SetSelectedGameObject(DefaultFocusTarget);
		}


		public async UniTask ShowAnimationAsync()
		{
			if (_isInAnimation)
			{
				Debug.LogError($"Can't play <Show> animation of <{name}> while another animation is playing");
				return;
			}
			
			_isInAnimation = true;
			
			await PlayShowAnimationAsync();
			
			_isInAnimation = false;
		}
		

		public async UniTask HideAnimationAsync()
		{
			if (_isInAnimation)
			{
				Debug.LogError($"Can't play <Hide> animation of <{name}> while another animation is playing");
				return;
			}
			
			_isInAnimation = true;
			
			await PlayHideAnimationAsync();
			
			_isInAnimation = false;
		}

		
		protected virtual async UniTask PlayShowAnimationAsync()
		{
			await UniTask.CompletedTask;
		}
		
		
		protected virtual async UniTask PlayHideAnimationAsync()
		{
			await UniTask.CompletedTask;
		}
		
		
		public async UniTask WaitForAnimationEndAsync(CancellationToken token = default)
		{
			await UniTask.DelayFrame(1, cancellationToken: token);
			
			while (_isInAnimation)
				await UniTask.DelayFrame(1, cancellationToken: token);
		}


		public void Validate(SelfValidationResult result)
		{
			if (DefaultFocusTarget == null && !IgnoreEmptyDefaultFocusTargetWarning)
				result.AddWarning("No default focus target provided");
		}
	}
}
