﻿using System.Threading;
using Cysharp.Threading.Tasks;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Valhalla.ScreenSystem
{
	public abstract class BaseScreen : NoParameterInfoScreen
	{
		
		public virtual void SetupOnInstantiate()
		{
			
		}

	}
	
	
	public abstract class BaseScreen<TArg> : NoParameterInfoScreen
	{
		public abstract void SetupOnInstantiate(TArg arg);
	}
}
