﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;


namespace Valhalla.ScreenSystem
{
	[Serializable]
	public class ScreenPrefabMap
	{
		[SerializeField, AssetsOnly, Required]
		private List<NoParameterInfoScreen> _screens = new List<NoParameterInfoScreen>();

		
		public TScreen GetPrefab<TScreen>()
			where TScreen : NoParameterInfoScreen
		{
			foreach (var baseScreen in _screens)
				if (baseScreen is TScreen screen)
					return screen;

			throw new IndexOutOfRangeException($"Can't find view of type {typeof(TScreen)}");
		}
	}
}
