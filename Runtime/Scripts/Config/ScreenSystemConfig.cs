using Sirenix.OdinInspector;
using UnityEngine;


namespace Valhalla.ScreenSystem
{
	[CreateAssetMenu(fileName = "screen_system_config", menuName = "Valhalla/Screen System/Config")]
	public class ScreenSystemConfig : ScriptableObject
	{
		[SerializeField, Required]
		[HideLabel, BoxGroup("Screen Prefabs")]
		private ScreenPrefabMap _screenPrefabMap = new();
		
		[SerializeField, Required]
		[HideLabel, BoxGroup("Layers")]
		private ScreenLayerOrder _screenLayerOrder = new();
		
		
		public ScreenPrefabMap ScreenPrefabMap 
			=> _screenPrefabMap;
		
		public ScreenLayerOrder ScreenLayerOrder 
			=> _screenLayerOrder;
	}
}
