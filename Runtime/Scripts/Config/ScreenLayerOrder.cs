using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Valhalla.ScreenSystem
{
    [Serializable]
    public class ScreenLayerOrder
    {
	    [SerializeField]
	    private int _offset = 0;
	    
	    
        [SerializeField, AssetsOnly, Required]
        [OnCollectionChanged(nameof(UpdateReversedList))]
        private List<ScreenLayer> _layers;
        
        
        private List<ScreenLayer> _layersReversed;

        
        public int this[ScreenLayer layer]
        {
            get
            {
                if (_layersReversed == null || _layersReversed.Count != _layers.Count)
                    UpdateReversedList();
                
                if (!_layersReversed.Contains(layer))
                {
                    Debug.LogError($"No layer {layer} in config");
                    return int.MaxValue;
                }
                
                return _layersReversed.IndexOf(layer) + _offset;
            }
        }
        
        
        [Button("Update Reversed List")]
        private void UpdateReversedList()
        {
            _layersReversed = new List<ScreenLayer>();
            _layersReversed.AddRange(_layers);
            _layersReversed.Reverse();
        }
    }
}