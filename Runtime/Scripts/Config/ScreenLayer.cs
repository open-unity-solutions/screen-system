﻿using UnityEngine;

namespace Valhalla.ScreenSystem
{
	[CreateAssetMenu(fileName = "screen_layer", menuName = "Valhalla/Screen System/Screen Layer")]
	public class ScreenLayer : ScriptableObject
	{

	}
}
