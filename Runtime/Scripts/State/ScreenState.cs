using System;
using System.Linq;
using JetBrains.Annotations;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;


namespace Valhalla.ScreenSystem.State
{
	[Serializable]
	public class ScreenState
	{
		[SerializeField, SceneObjectsOnly]
		[CanBeNull]
		private NoParameterInfoScreen _screen = null;

		
		[CanBeNull]
		public object Arg = null;
		
		
		[FormerlySerializedAs("_state")]
		[SerializeField]
		private ScreenDisplayStage _displayStage = ScreenDisplayStage.Hidden;


		public NoParameterInfoScreen Screen
			=> _screen;
		
		
		public ScreenDisplayStage DisplayStage
			=> _displayStage;


		private string ScreenTypeName
			=> _screen != null 
				? _screen.GetType().Name 
				: null;


		public ScreenState()
		{
			_screen = null;
			_displayStage = ScreenDisplayStage.Hidden;
		}


		public ScreenState(NoParameterInfoScreen screen, ScreenDisplayStage displayStage)
		{
			_screen = screen;
			_displayStage = displayStage;
		}
		

		public void OnShowStarted(NoParameterInfoScreen screen)
		{
			CanBeOnlyInStates($"Can't start showing screen <{ScreenTypeName}> in state {_displayStage}", ScreenDisplayStage.Hidden);
			
			_screen = screen;
			_displayStage = ScreenDisplayStage.InProcessOfShow;
		}
		
		
		public void OnShowFinished()
		{
			CanBeOnlyInStates($"Can't finish showing screen <{ScreenTypeName}> in state {_displayStage}", ScreenDisplayStage.InProcessOfShow);

			_displayStage = ScreenDisplayStage.ShowedAndDisplaying;
		}


		public void OnHideStarted()
		{
			CanBeOnlyInStates($"Can't start hiding screen <{ScreenTypeName}> in state {_displayStage}", ScreenDisplayStage.ShowedAndDisplaying);

			_displayStage = ScreenDisplayStage.InProcessOfHide;
		}


		public void OnHideFinished()
		{
			CanBeOnlyInStates($"Can't finish hiding screen <{ScreenTypeName}> in state {_displayStage}", ScreenDisplayStage.InProcessOfHide);

			_screen = null;
			_displayStage = ScreenDisplayStage.Hidden;
		}


		private void CanBeOnlyInStates(string errorMessage, params ScreenDisplayStage[] validStates)
		{
			if (!validStates.Contains(_displayStage))
				throw new Exception(errorMessage);
		}
	}
}
