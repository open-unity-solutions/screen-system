namespace Valhalla.ScreenSystem.State
{
	public enum ScreenDisplayStage
	{
		Hidden = 0,
		ShowedAndDisplaying,
		InProcessOfShow,
		InProcessOfHide,
	}
}
