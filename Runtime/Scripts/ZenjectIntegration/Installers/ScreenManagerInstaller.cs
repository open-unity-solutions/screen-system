#if ZENJECT_ENABLED

using Sirenix.OdinInspector;
using UnityEngine;
using Zenject;


namespace Valhalla.ScreenSystem.ZenjectInstallers
{
	public class ScreenManagerInstaller : MonoInstaller
	{
		[SerializeField, SceneObjectsOnly, Required]
		private ScreenManager _screenManager;
		
		
		public override void InstallBindings()
		{
			Container
				.Bind<ScreenManager>()
				.FromInstance(_screenManager);
		}
	}
}

#endif
